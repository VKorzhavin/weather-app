package ru.korzhavin.weatherapp.model;

import lombok.Data;
import ru.korzhavin.weatherapp.openweathermap.model.Coord;
import ru.korzhavin.weatherapp.openweathermap.model.TemperatureDetails;
import ru.korzhavin.weatherapp.openweathermap.model.Weather;
import ru.korzhavin.weatherapp.openweathermap.model.Wind;

import java.util.List;

@Data
public class MyWeather {

    private Integer id;
    private String name;
    private String code;
    private Coord coord;
    private List<Weather> weather;
    private String base;
    private TemperatureDetails main;
    private String visibility;
    private Wind wind;

}
