package ru.korzhavin.weatherapp.openweathermap.model;

import lombok.Data;

@Data
public class TemperatureDetails {

    private String temp;
    private String pressure;
    private String humidity;
    private String temp_min;
    private String temp_max;
}
