package ru.korzhavin.weatherapp.openweathermap.model;

import lombok.Data;

@Data
public class Wind {

    private String speed;
    private String deg;
}
