package ru.korzhavin.weatherapp.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.korzhavin.weatherapp.model.MyWeather;
import ru.korzhavin.weatherapp.service.MyWeatherService;

import static java.lang.String.format;

@Slf4j
@RestController
@RequestMapping("/weather")
public class WeatherController {

    @Autowired
    MyWeatherService myWeatherService;

    @GetMapping(value = "/city")
    public MyWeather getByCity(@RequestParam(name = "name") String name) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        log.info(format("User '%s' find weather in city %s", auth.getName(),  name));
        MyWeather weather = myWeatherService.getByCity(name);
        log.info(format("User '%s' get result: %s", auth.getName(), weather.toString()));
        return weather;
    }

    @GetMapping(value = "/coord")
    public MyWeather getByCoord(@RequestParam(name = "lat") Double lat, @RequestParam(name = "lon") Double lon) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        log.info(format("User '%s' find weather in coord{%s ; %s}", auth.getName(),  lat, lon));
        MyWeather weather = myWeatherService.getByCoord(lat, lon);
        log.info(format("User '%s' get result: %s", auth.getName(), weather.toString()));
        return weather;
    }
}
