package ru.korzhavin.weatherapp.controller;

import feign.FeignException;
import feign.RetryableException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import static java.lang.String.format;

@Slf4j
@RestControllerAdvice
public class WeatherExceptionHandler {

    @ExceptionHandler(FeignException.class)
    public ResponseEntity<String> handle(FeignException ex) {
        log.error(ex.getMessage());
        log.info(format("User '%s' get exception: %s", SecurityContextHolder.getContext().getAuthentication().getName(), ex.getMessage()));
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Данных не найдено");
    }

    @ExceptionHandler(RetryableException.class)
    public ResponseEntity<String> handle(RetryableException ex) {
        log.error(ex.getMessage());
        log.info(format("User '%s' get exception: %s", SecurityContextHolder.getContext().getAuthentication().getName(), ex.getMessage()));
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Сервис погоды временно недоступен");
    }

}
