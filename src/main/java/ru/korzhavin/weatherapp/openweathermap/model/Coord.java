package ru.korzhavin.weatherapp.openweathermap.model;

import lombok.Data;

@Data
public class Coord {

    private String lon; //долгота
    private String lat; //широта
}
