package ru.korzhavin.weatherapp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.korzhavin.weatherapp.model.MyWeather;
import ru.korzhavin.weatherapp.openweathermap.service.IOpenWeatherMapService;

@Service
@PropertySource("classpath:application.properties")
public class MyWeatherService {

    @Value("${openWeatherMap.appid}")
    private String APP_ID;

    @Value("${openWeatherMap.unit}")
    private String UNIT;

    @Autowired
    IOpenWeatherMapService mapService;

    public MyWeather getByCity(String city) {
        return mapService.getWeatherByCity(city, APP_ID, UNIT);
    }

    public MyWeather getByCoord(Double lat, Double lon) {
        return mapService.getWeatherByCoord(lat, lon, APP_ID, UNIT);
    }
}
