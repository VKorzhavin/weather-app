package ru.korzhavin.weatherapp.openweathermap.service;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.korzhavin.weatherapp.model.MyWeather;

@FeignClient(name = "openWeatherMap", url = "http://api.openweathermap.org")
public interface IOpenWeatherMapService {

    String WEATHER_API_URL = "data/2.5/weather";

    @GetMapping(value = WEATHER_API_URL)
    MyWeather getWeatherByCity(@RequestParam(name = "q") String city
            , @RequestParam(name = "appid") String appid
            , @RequestParam(name = "units") String units);

    @GetMapping(value = WEATHER_API_URL)
    MyWeather getWeatherByCoord(@RequestParam(name = "lat") Double lat
            , @RequestParam(name = "lon") Double lon
            , @RequestParam(name = "appid") String appid
            , @RequestParam(name = "units") String units);
}
