package ru.korzhavin.weatherapp.openweathermap.model;

import lombok.Data;

@Data
public class Weather {

    private String id;
    private String main;
    private String description;
    private String icon;
}
