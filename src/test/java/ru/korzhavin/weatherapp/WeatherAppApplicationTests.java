package ru.korzhavin.weatherapp;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
public class WeatherAppApplicationTests {

	private MockMvc mockMvc;

	@Autowired
	private WebApplicationContext webApplicationContext;

	@Before
	public void setUp() {
		mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext)
				.apply(SecurityMockMvcConfigurers.springSecurity()).build();
	}

	@Test
	public void testByCityEN() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get("/weather/city")
				.with(SecurityMockMvcRequestPostProcessors
						.httpBasic("vadim", "123"))
				.param("name", "kirov"))
				.andExpect(jsonPath("$.name", Matchers.equalTo("Kirov")));
	}

	@Test
	public void testByCityRU() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get("/weather/city")
				.with(SecurityMockMvcRequestPostProcessors
						.httpBasic("vadim", "123"))
				.param("name", "Киров"))
				.andExpect(jsonPath("$.name", Matchers.equalTo("Kirov")));
	}

	@Test
	public void testByCoord() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get("/weather/coord")
				.with(SecurityMockMvcRequestPostProcessors
						.httpBasic("vadim", "123"))
				.param("lon", "49.66")
				.param("lat", "58.6"))
				.andExpect(jsonPath("$.name", Matchers.equalTo("Kirov")));
	}

	@Test
	public void testNonAuthentication() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.get("/weather/city")
				.param("name", "Киров"))
				.andExpect(jsonPath("$", Matchers.equalTo("HTTP Status 401 - Full authentication is required to access this resource")));
	}

}
